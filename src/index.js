import React from 'react';
import Footer from './Footer'
import ReactDOM from 'react-dom';
import './index.scss';
import ContactInfo from './ContactInfo';
import registerServiceWorker from './registerServiceWorker';
class App extends React.Component{
      render(){
          return(
              <div>              
              <ContactInfo/>
              <Footer/>
              </div>
          );
      }
}
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
