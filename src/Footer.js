import React,{Component} from 'react';
import './Footer.scss'
class Footer extends Component{

    render(){
        return(
         <div className="Footer">
         <div className="COPYRIGHT-CONTENT">COPYRIGHT © 2018, ALL RIGHTS RESERVED. INDIAVIDUAL PVT. LTD.</div>
         <div className="Powered-By-Content">Powered by EMBIBE</div>
         </div>
        );
    }
}
export default Footer;