import React, { Component } from 'react';
import './ContactInfo.scss';

class ContactInfo extends Component {
  render() {
    return (
         <div className="ContactContainer">
           <div className="ContactUs">Contact-Us</div>
           <div className="PathBorder"></div>
           <div className="ContactInfoContainer">
               <div className="LocationImage"><img src={require('./image/location.png')}/></div>
               <div className="Aakash-Institute-Co">
                     Aakash Institute, Corporate Office
                     Aakash Tower, 8, 
                     Pusa Road, New Delhi, 
                     Delhi 110005
               </div>
               <div className="Email-Phone-Info-Box">
                   <div className="EmailInfoBox">
                        <div className="EmailImage"><img src={require('./image/email.png')}/></div>
                        <div className="EmailContent">
                          Medical@aesl.in
                          iitjee@aesl.in
                        </div>
                   </div>
                   <div className="PhoneInfoBox">
                        <div className="PhoneImage"><img src={require('./image/phone.png')}/></div>
                        <div className="PhoneContent">
                          (011)-47623456
                          (011)-47623472
                        </div>
                   </div>
               </div>
           </div>
         </div>
    );
  }
}
export default ContactInfo;
